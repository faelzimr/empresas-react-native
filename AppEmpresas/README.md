﻿# Desafio React Native - ioasys

Este documento `README.md` tem como objetivo fornecer as informações sobre o desenvolvimento do projeto Empresas.

---

### Bibliotecas utilizadas ###

* react-navigation: Utilizada para realizar a navegação entre as telas.
* prop-types: A biblioteca prop-types foi utilizada para não ter a opção voltar para o login.
* apisauce: Essa biblioteca foi usada para fazer a comunicação com a API EMPRESAS.


### Executar o Projeto utilizando o emulador do android ###

Após realizar o comando git pull para baixar os arquivos pelo git, com o emulador aberto execute o comando react-native run-android na pasta do projeto.