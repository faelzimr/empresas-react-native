import { AsyncStorage } from 'react-native'
import { create } from 'apisauce';

const api = create({
    baseURL: ' http://empresas.ioasys.com.br/api/v1',
});

api.addAsyncRequestTransform ( request => async () => {
    const access_token = await AsyncStorage.getItem('@APPEmpresas:access_token');
    const client = await AsyncStorage.getItem('@APPEmpresas:client');
    const uid = await AsyncStorage.getItem('@APPEmpresas:uid');

    if (access_token && client && uid){
        request.headers['client'] = `${client}`;
        request.headers['access-token'] = `${access_token}`;
        request.headers['uid'] = `${uid}`;
    }

});

api.addResponseTransform(response => {
    if (!response.ok) throw response;
});

export default api;