import React , {Component} from 'react';
import api from '../services/api';
import { View, Text, FlatList, TouchableOpacity, StyleSheet } from 'react-native';

export default class Main extends Component {
	static navigationOptions = {
		title: "Main"
	};

	state = {
        errorMessage: null,
        loggedInUser: null,
        enterprises: [],
    }

	componentDidMount(){
		this.getEnterpriseList();
    }
    
    getEnterpriseList = async () => {
        try{
          const response = await api.get('/enterprises');
    
          const { enterprises } = response.data;
    
          this.setState({ enterprises });
    
        } catch (response){
          this.setState({ errorMessage: response.data.errors});
        }
    };

	renderItem = ({ item }) => (
		<View style={styles.enterpriseContainer}> 
			<Text style={styles.enterpriseTitle}>{item.enterprise_name}</Text>
			<Text style={styles.enterprisesDescription}>{item.enterprise_type.enterprise_type_name}</Text>

			<TouchableOpacity 
				style={styles.enterpriseButton} 
				onPress={() => {
					this.props.navigation.navigate("Enterprise", {id: item.id});
				}}
			>
				<Text style={styles.enterpriseButtonText}>Acessar</Text>
			</TouchableOpacity>
		</View>
	)

	render() {
		return (
			<View style={styles.container}>
				<FlatList 
					contentContainerStyle={styles.list}
					data={this.state.enterprises} 
					keyExtractor={item => String(item.id)}
                    renderItem={this.renderItem}
                    initialNumToRender={10}
					onEndReachedThreshold={0.1}
                />               

			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fafafa"
	},

	list: {
		padding: 20
	},

	enterpriseContainer: {
		backgroundColor: "#FFF",
		borderWidth: 1,
		borderColor: "#DDD",
		borderRadius: 5,
		padding: 20,
		marginBottom: 20
	},

	enterpriseTitle: {
		fontSize: 18,
		fontWeight: "bold",
		color: "#333"
	},
	enterprisesDescription: {
		fontSize: 16,
		color: "#999",
		marginTop: 5,
		lineHeight: 24
	},
	enterpriseButton: {
		height: 42,
		borderRadius: 5,
		borderWidth: 2,
		borderColor: "#DA552F",
		backgroundColor: "transparent",
		justifyContent: "center",
		alignItems: "center",
		marginTop: 10
	},
	enterpriseButtonText: {
		fontSize: 16,
		color: "#DA552F",
		fontWeight: "bold"
	}
});