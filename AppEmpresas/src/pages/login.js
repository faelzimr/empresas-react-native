import React, {Component} from 'react';

import PropTypes from 'prop-types';

import { StackActions, NavigationActions } from 'react-navigation';

import { View, Text, TextInput, TouchableOpacity, AsyncStorage, StyleSheet } from "react-native";

import api from '../services/api';

export default class Login extends Component {
    static navigationOptions = {
		title: "APPEmpresas"
    };

    static propTypes = {
        navigation: PropTypes.shape({
            navigate: PropTypes.func,
            dispatch: PropTypes.func,
        }).isRequired,
    };
    
    state = {
        email: null,
        password: null,
        loggedInUser: null,
        errorMessage: null,
    }

    handleEmail = (text) => {
        this.setState({ email: text })
    }
        handlePassword = (text) => {
        this.setState({ password: text })
    }

    signIn = async () => {
        try {
            
            const response = await api.post('/users/auth/sign_in', {
            email: this.state.email,
            password: this.state.password,
            });
            
            
            const { client, uid  } = response.headers;
            const { investor } = response.data;
            const access_token = response.headers['access-token'];

            await AsyncStorage.multiSet([
            ['@APPEmpresas:access_token', access_token],
            ['@APPEmpresas:client', client],
            ['@APPEmpresas:uid', uid],
            ['@APPEmpresas:investor', JSON.stringify(investor)],
            ]);

            this.setState({ loggedInUser: investor});

            const resetAction = StackActions.reset({
                index: 0,
                actions: [
                NavigationActions.navigate({ routeName: 'Main' }),
                ],
            });
            this.props.navigation.dispatch(resetAction);
        } catch (response){
            this.setState({errorMessage: response.data.errors});
        }
    };
    
    render(){
        return (
            <View style={styles.container}>                
                <Text style={styles.title}>Login</Text>                
                    <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Email"
                    placeholderTextColor = "#DDD"
                    autoCapitalize = "none"
                    onChangeText = {this.handleEmail}
                />
                
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Password"
                    placeholderTextColor = "#DDD"
                    autoCapitalize = "none"
                    onChangeText = {this.handlePassword}
                />
                
                <TouchableOpacity
                    style = {styles.submitButton}
                    onPress = {this.signIn
                    }
                >
                    <Text style = {styles.submitButtonText}> Entrar </Text>
                </TouchableOpacity>

                { !!this.state.errorMessage  && <Text>{ this.state.errorMessage }</Text>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: "#fafafa",
      padding: 20
    },
    title: {
		fontSize: 18,
		fontWeight: "bold",
        color: "#333",
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
	},
    input: {
        marginBottom: 10,
        height: 40,
        borderColor: '#DDD',
        borderWidth: 1,
        color: "#333"
     },
     submitButton: {
        height: 42,
		borderRadius: 5,
		borderWidth: 2,
		borderColor: "#DA552F",
		backgroundColor: "transparent",
		justifyContent: "center",
		alignItems: "center"
     },
     submitButtonText:{
        fontSize: 16,
		color: "#DA552F",
		fontWeight: "bold"
     }
  });