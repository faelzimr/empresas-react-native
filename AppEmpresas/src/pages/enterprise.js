import React , {Component} from 'react';
import api from '../services/api';
import { View, Text, StyleSheet} from 'react-native';

export default class Enterprise extends Component {
    static navigationOptions = {
		title: "Enterprise"
    };
    
    state = {
        errorMessage: null,
        loggedInUser: null,
        enterprise: [],
        id: null,
    }
    componentDidMount(){        
        this.getEnterprise();
    }
    getEnterprise = async () => {
        try{
          id = this.props.navigation.state.params.id;
          const response = await api.get(`/enterprises/${id}`);
    
          const { enterprise } = response.data;

          this.setState({ enterprise });
    
        } catch (response){
          this.setState({ errorMessage: response.data.errors});
        }
    };   
    
    render() {
		return (
            <View style={styles.container}>
                <View style={styles.enterpriseContainer}> 
                    { !!this.state.enterprise.enterprise_name  && <Text style={styles.enterpriseTitle}>{ this.state.enterprise.enterprise_name }</Text>}
                    { !!this.state.enterprise.email_enterprise  && <Text style={styles.enterprisesDescription}>E-mail: { this.state.enterprise.email_enterprise }</Text>}
                    { !!this.state.enterprise.facebook  && <Text style={styles.enterprisesDescription}>Facebook: { this.state.enterprise.facebook }</Text>}
                    { !!this.state.enterprise.twitter  && <Text style={styles.enterprisesDescription}>Twitter: { this.state.enterprise.twitter }</Text>}
                    { !!this.state.enterprise.linkedin  && <Text style={styles.enterprisesDescription}>Linkedin: { this.state.enterprise.linkedin }</Text>}
                    { !!this.state.enterprise.phone  && <Text style={styles.enterprisesDescription}>Phone: { this.state.enterprise.phone }</Text>}
                    { !!this.state.enterprise.share_price  && <Text style={styles.enterprisesDescription}>Share price: { this.state.enterprise.share_price }</Text>}
                    { !!this.state.enterprise.city  && <Text style={styles.enterprisesDescription}>City: { this.state.enterprise.city }</Text>}
                    { !!this.state.enterprise.country  && <Text style={styles.enterprisesDescription}>Country: { this.state.enterprise.country }</Text>}                    
                    { !!this.state.enterprise.description  && <Text style={styles.enterprisesDescription}>{ this.state.enterprise.description }</Text>}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fafafa"
	},
	enterpriseContainer: {
		backgroundColor: "#FFF",
		borderWidth: 1,
		borderColor: "#DDD",
		borderRadius: 5,
		padding: 20,
		marginBottom: 20
	},

	enterpriseTitle: {
		fontSize: 18,
		fontWeight: "bold",
		color: "#333"
	},
	enterprisesDescription: {
		fontSize: 16,
		color: "#999",
		marginTop: 5,
		lineHeight: 24
	}
});