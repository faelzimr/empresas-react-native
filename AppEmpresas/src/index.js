import React from 'react';
import Routes from './routes';
import "./config/StatusBarConfig";

const APP = () => <Routes />;

export default APP;