import { createStackNavigator } from 'react-navigation';

import Login from "./pages/login";
import Main from "./pages/main";
import Enterprise from "./pages/enterprise";


export default createStackNavigator({
    Login,
    Main,
    Enterprise,
}, {
	navigationOptions: {
		headerStyle: {
			backgroundColor: "#DA552F"
		},
		headerTintColor: "#FFF"
	},
});