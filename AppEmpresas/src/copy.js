import React, {Component} from 'react';
import {
  Platform, 
  StyleSheet, 
  Text, 
  View,
  Button,
  AsyncStorage,
  Alert,
} from 'react-native';

import api from './services/api';

export default class App extends Component {
  state = {
    errorMessage: null,
    loggedInUser: null,
    enterprises: [],
  }
  
  signIn = async () => {
    try {
      const response = await api.post('/users/auth/sign_in', {
        email: 'testeapple@ioasys.com.br',
        password: '12341234',
      });
      
      
      const { client, uid  } = response.headers;
      const { investor } = response.data;
      const access_token = response.headers['access-token'];

      await AsyncStorage.multiSet([
        ['@APPEmpresas:access_token', access_token],
        ['@APPEmpresas:client', client],
        ['@APPEmpresas:uid', uid],
        ['@APPEmpresas:investor', JSON.stringify(investor)],
      ]);

      this.setState({ loggedInUser: investor});

      Alert.alert('Login com sucesso!');
    } catch (response){
      this.setState({errorMessage: response.data.errors});
    }
  };

  getEnterpriseList = async () => {
    try{
      const response = await api.get('/enterprises');

      const { enterprises } = response.data;

      this.setState({ enterprises });

    } catch (response){
      this.setState({ errorMessage: response.data.errors});
    }
  };

  async componentDidMount(){
    const access_token = await AsyncStorage.getItem('@APPEmpresas:access_token');
    const client = await AsyncStorage.getItem('@APPEmpresas:client');
    const uid = await AsyncStorage.getItem('@APPEmpresas:uid');

    const investor = JSON.parse(await AsyncStorage.getItem('@APPEmpresas:investor'));

    if (client && investor && uid && access_token)
      this.setState({ loggedInUser:investor });

  }

  
  render() {
    return (
      <View style={styles.container}>
        { !!this.state.loggedInUser  && <Text>{ this.state.loggedInUser.investor_name }</Text>}
        { !!this.state.errorMessage  && <Text>{ this.state.errorMessage }</Text>}
        
        { this.state.loggedInUser 
          ? <Button onPress={this.getEnterpriseList} title="Carregar" />            
          : <Button onPress={this.signIn} title="Entrar" />        
        }

        

        { this.state.enterprises.map( entreprise => (
          <View key={entreprise.id} style={{ marginTop: 15}}>
            <Text style={{fontWeight: 'bold'}}>{entreprise.enterprise_name}</Text>
            <Text>{entreprise.description}</Text>
          </View>
        )) }
         
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
